﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public class BuzzerManager : GenericManager<Buzzer>, IBuzzerManager
    {
        public BuzzerManager(IGenericRepository<Buzzer> repository) : base(repository)
        {
        }
    }
}
