﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public class DispositivoManager : GenericManager<Dispositivo>, IDispositivoManager
    {
        IGenericRepository<Dispositivo> repository;
        public DispositivoManager(IGenericRepository<Dispositivo> repository) : base(repository)
        {
            this.repository = repository;
        }

        public Dispositivo ActualizarAnguloServomotor(ObjectId idDispositivo, Servomotor servomotor)
        {
            Dispositivo d = BuscarPorId(idDispositivo);
            d.EstadoServomotor = servomotor;
            return Actualizar(d);
        }

        public Dispositivo ActualizarEstadoBuzzer(ObjectId idDispositivo, Buzzer buzzer)
        {
            Dispositivo d = BuscarPorId(idDispositivo);
            d.EstadoBuzer = buzzer;
            return Actualizar(d);
        }

        public Dispositivo ActualizarEstadoLed(ObjectId idDispositivo, Led led)
        {
            Dispositivo d = BuscarPorId(idDispositivo);
            d.EstadoLed = led;
            return Actualizar(d);
        }

        public Dispositivo ActualizarEstadoPulsador(ObjectId idDispositivo, Pulsador pulsador)
        {
            Dispositivo d = BuscarPorId(idDispositivo);
            d.ValorPulsador = pulsador;
            return Actualizar(d);
        }

        public Dispositivo ActualizarLecturaSensores(ObjectId idDispositivo, LecturaSensores lecturaSensores)
        {
            Dispositivo d = BuscarPorId(idDispositivo);
            d.UltimaLectura = lecturaSensores;
            return Actualizar(d);
        }

        public IEnumerable<Dispositivo> DispositivosDeUsuario(ObjectId idUsuario)
        {
            return repository.Query(d => d.IdUsuario == idUsuario);
        }
    }
}
