﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using CursoIoT.COMMON.Validadores;
using CursoIoT.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public static class FabricManager
    {
        public static IUsuarioManager UsuarioManager()
        {
            return new UsuarioManager(new GenericRepository<Usuario>(new UsuarioValidator()));
        }
        public static IDispositivoManager DispositivoManager()
        {
            return new DispositivoManager(new GenericRepository<Dispositivo>(new DispositivoValidator()));
        }
        public static ILecturaSensorManager LecturaSensorManager()
        {
            return new LecturaSensorManager(new GenericRepository<LecturaSensores>(new LecturaSensorValidator()));
        }
        public static ILedManager LedManager()
        {
            return new LedManager(new GenericRepository<Led>(new LedValidator()));
        }
        public static IPulsadorManager PulsadorManager()
        {
            return new PulsadorManager(new GenericRepository<Pulsador>(new PulsadorValidator()));
        }
        public static IBuzzerManager BuzzerManager()
        {
            return new BuzzerManager(new GenericRepository<Buzzer>(new BuzzerValidator()));
        }
        public static IServoMotorManager ServoMotorManager()
        {
            return new ServoMotorManager(new GenericRepository<Servomotor>(new ServoMotorValidator()));
        }
    }
}
