﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CursoIoT.BIZ
{
    public abstract class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> repository)
        {
            this.repository = repository;
        }
        public bool Resultado { get => repository.Resultado; set => repository.Resultado=value; }
        public string Error { get => repository.Error; set => repository.Error = value; }

        public IQueryable<T> Listar => repository.Read.AsQueryable();

        public T Actualizar(T entidad)
        {
            return repository.Update(entidad);
        }

        public T BuscarPorId(ObjectId id)
        {
            return repository.SearchById(id);
        }

        public bool Eliminar(ObjectId id)
        {
            return repository.Delete(id);
        }

        public bool EliminarTodos(Expression<Func<T, bool>> predicado)
        {
            return repository.DeleteAll(predicado);
        }

        public T Insertar(T entidad)
        {
            return repository.Create(entidad);
        }

        public IQueryable<T> InsertarTodos(IQueryable<T> entidades)
        {
            return repository.CreateAll(entidades.AsEnumerable()).AsQueryable();
        }

        public IQueryable<T> ListarPaginado(int tamanioPagina, int numeroPagina)
        {
            return repository.Read.Skip(tamanioPagina * (numeroPagina - 1)).Take(tamanioPagina).AsQueryable();
        }

        public IQueryable<T> ListarPaginadoPersonalizado(int omitir = -1, int mostrar = -1)
        {
            if (omitir == -1)
            {
                if (mostrar == -1)
                {
                    return repository.Read.AsQueryable();
                }
                else
                {
                    return repository.Read.Take(mostrar).AsQueryable();
                }
            }
            else
            {
                if (mostrar == -1)
                {
                    return repository.Read.Skip(omitir).AsQueryable();
                }
                else
                {
                    return repository.Read.Skip(omitir).Take(mostrar).AsQueryable();
                }
            }
            
        }
    }
}
