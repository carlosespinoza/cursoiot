﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public class LecturaSensorManager : GenericManager<LecturaSensores>, ILecturaSensorManager
    {
        public LecturaSensorManager(IGenericRepository<LecturaSensores> repository) : base(repository)
        {
        }
    }
}
