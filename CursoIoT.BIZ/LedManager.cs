﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public class LedManager : GenericManager<Led>, ILedManager
    {
        public LedManager(IGenericRepository<Led> repository) : base(repository)
        {
        }

       
    }
}
