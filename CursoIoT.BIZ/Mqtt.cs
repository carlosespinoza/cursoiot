﻿using CursoIoT.COMMON.Entidades;
using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace CursoIoT.BIZ
{
    public class Mqtt
    {
        public bool ServidorMqttNoConectado { get; set; }
        public event EventHandler<string> MensajeRecibido;
        public string Server { get; private set; }
        public int Puerto { get; private set; }
        public string Topic { get; private set; }
        public string IdDispositivo { get; private set; }
        private MQTTClient client;
        private int intentos;
        public Mqtt(string server, int puerto, string topic, string IdDispositivo,int cantIntentos=30)
        {
            Server = server;
            Puerto = puerto;
            Topic = topic + "/" + IdDispositivo;
            intentos = 0;
            this.IdDispositivo = IdDispositivo;
            client = new MQTTClient(server, puerto);
            if (IdDispositivo.Length > 20)
            {
                client.Connect("test");
            }
            else
            {
                client.Connect(IdDispositivo);
            }
            Escribe("Intentando conectar...");
            Debug.WriteLine($"[{this.IdDispositivo}] Intentando conectar...");
            while (!client.IsConnected)
            {
                Thread.Sleep(1000);
                Escribe(".");
                intentos++;
                if (intentos == cantIntentos)
                {
                    Escribe("No se pudo conectar, revise la configuración o que el servidor este vivo");
                    break;
                }
            }
            if (client.IsConnected)
            {
                Escribe("Conectado correctamente :)");
                ServidorMqttNoConectado = false;
            }
            else
            {
                Escribe("Error al conectar :'(");
                ServidorMqttNoConectado = true;
            }
        }

        private void Escribe(string mensaje)
        {
            Debug.WriteLine($"[{IdDispositivo}] {mensaje}");
        }
        public void Publica(string mensaje)
        {
            if (client.IsConnected)
            {
                client.Publish(Topic, mensaje, QoS.FireAndForget, false);
                Escribe("Mensaje enviado: " + mensaje);
            }
            else
            {
                Escribe("El cliente no esta conectado, no se puede enviar el mensaje");
            }
        }

        public void Suscribirse(string topic)
        {
            if (client.IsConnected)
            {
                client.Subscriptions.Add(new Subscription(topic));
                client.MessageReceived += Client_MessageReceived;
            }
        }

        private void Client_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            string m = System.Text.Encoding.UTF8.GetString(payload);
            Escribe($"{topic}:{m}");
            MensajeRecibido(this, m);
            
        }

        public void Desconectar()
        {
            client.Disconnect();
        }
    }
}
