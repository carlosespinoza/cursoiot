﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public class PulsadorManager : GenericManager<Pulsador>, IPulsadorManager
    {
        public PulsadorManager(IGenericRepository<Pulsador> repository) : base(repository)
        {
        }
    }
}
