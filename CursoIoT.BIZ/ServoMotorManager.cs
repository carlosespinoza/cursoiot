﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.BIZ
{
    public class ServoMotorManager : GenericManager<Servomotor>, IServoMotorManager
    {
        public ServoMotorManager(IGenericRepository<Servomotor> repository) : base(repository)
        {
        }
    }
}
