﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CursoIoT.BIZ
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuarioManager
    {
        IGenericRepository<Usuario> repository;
        public UsuarioManager(IGenericRepository<Usuario> repository) : base(repository)
        {
            this.repository = repository;
        }

        public Usuario Login(string nombreUsuario, string password)
        {
            return repository.Query(u => u.UserName == nombreUsuario && u.Password == password).SingleOrDefault();
        }
    }
}
