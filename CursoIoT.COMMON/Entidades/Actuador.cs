﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public abstract class Actuador:BaseDTO
    {
        public ObjectId IdDispositivo { get; set; }
        public string Origen { get; set; }
    }
}
