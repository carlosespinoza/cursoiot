﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public abstract class BaseDTO : IDisposable
    {
        public ObjectId Id { get; set; }
        public DateTime FechaHora { get; set; }
        private bool isDisposed;
        public void Dispose()
        {
            if (!isDisposed)
            {
                this.isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
    }
}
