﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public class Dispositivo : BaseDTO
    {
        public ObjectId IdUsuario { get; set; }
        public float Latitud { get; set; }
        public float Longitud { get; set; }
        public string Descripcion { get; set; }
        public LecturaSensores UltimaLectura { get; set; }
        public Pulsador ValorPulsador { get; set; }
        public Led EstadoLed { get; set; }
        public Buzzer EstadoBuzer { get; set; }
        public Servomotor EstadoServomotor { get; set; }
        public override string ToString()
        {
            return $"[{Id}] {Descripcion}";
        }
    }
}
