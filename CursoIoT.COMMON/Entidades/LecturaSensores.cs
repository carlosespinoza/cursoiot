﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public class LecturaSensores:BaseDTO
    {
        public ObjectId IdDispositivo { get; set; }
        public float Temperatura { get; set; }
        public float Humedad { get; set; }
        public float Luminosidad { get; set; }
    }
}
