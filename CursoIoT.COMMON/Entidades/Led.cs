﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public class Led:Actuador
    {
        public bool Encendido { get; set; }
    }
}
