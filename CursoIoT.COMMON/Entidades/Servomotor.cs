﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public class Servomotor:Actuador
    {
        public float Angulo { get; set; }
    }
}
