﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Entidades
{
    public class Usuario : BaseDTO
    {
        public string UserName { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
