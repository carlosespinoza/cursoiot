﻿using CursoIoT.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Interfaces
{
    public interface IDispositivoManager:IGenericManager<Dispositivo>
    {
        Dispositivo ActualizarEstadoLed(ObjectId idDispositivo, Led led);
        Dispositivo ActualizarEstadoBuzzer(ObjectId idDispositivo, Buzzer buzzer);
        Dispositivo ActualizarEstadoPulsador(ObjectId idDispositivo, Pulsador pulsador);
        Dispositivo ActualizarAnguloServomotor(ObjectId idDispositivo, Servomotor servomotor);
        Dispositivo ActualizarLecturaSensores(ObjectId idDispositivo, LecturaSensores lecturaSensores);
        IEnumerable<Dispositivo> DispositivosDeUsuario(ObjectId idUsuario);
    }
}
