﻿using CursoIoT.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CursoIoT.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        bool Resultado { get; set; }
        string Error { get; set; }
        T Insertar(T entidad);
        IQueryable<T> Listar { get; }
        T Actualizar(T entidad);
        bool Eliminar(ObjectId id);
        T BuscarPorId(ObjectId id);
        bool EliminarTodos(Expression<Func<T, bool>> predicado);
        IQueryable<T> InsertarTodos(IQueryable<T> entidades);
        IQueryable<T> ListarPaginadoPersonalizado(int omitir = -1, int mostrar = -1);
        IQueryable<T> ListarPaginado(int tamanioPagina, int numeroPagina);
    }
}
