﻿using CursoIoT.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace CursoIoT.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        bool Resultado { get; set; }
        string Error { get; set; }
        T Create(T entidad);
        T Update(T entidad);
        IEnumerable<T> Read { get; }
        bool Delete(ObjectId id);
        T SearchById(ObjectId id);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
        bool DeleteAll(Expression<Func<T, bool>> predicado);
        IEnumerable<T> CreateAll(IEnumerable<T> entidades);


    }
}
