﻿using CursoIoT.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Interfaces
{
    public interface IPulsadorManager:IGenericManager<Pulsador>
    {
    }
}
