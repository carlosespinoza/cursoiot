﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public abstract class ActuadorValidator<T>:GenericValidator<T> where T:Actuador
    {
        public ActuadorValidator()
        {
            RuleFor(a => a.IdDispositivo).NotNull().NotEmpty();
            RuleFor(a => a.Origen).NotEmpty().NotNull();
        }
    }
}
