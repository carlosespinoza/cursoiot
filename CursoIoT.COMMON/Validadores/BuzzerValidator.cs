﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class BuzzerValidator:ActuadorValidator<Buzzer>
    {
        public BuzzerValidator()
        {
            RuleFor(b => b.Encendido).NotNull().NotEmpty();
        }
    }
}
