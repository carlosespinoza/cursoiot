﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class DispositivoValidator:GenericValidator<Dispositivo>
    {
        public DispositivoValidator()
        {
            RuleFor(d => d.Descripcion).NotNull().NotEmpty();
            RuleFor(d => d.IdUsuario).NotEmpty().NotNull();
            RuleFor(d => d.Latitud).NotNull().NotEmpty();
            RuleFor(d => d.Longitud).NotEmpty().NotNull();
        }
    }
}
