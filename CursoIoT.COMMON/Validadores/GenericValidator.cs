﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public abstract class GenericValidator<T>:AbstractValidator<T> where T:BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(p => p.Id).NotNull().NotEmpty();
            RuleFor(p => p.FechaHora).NotEmpty().NotNull();
        }
    }
}
