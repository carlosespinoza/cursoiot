﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class LecturaSensorValidator:GenericValidator<LecturaSensores>
    {
        public LecturaSensorValidator()
        {
            RuleFor(l => l.Humedad).NotNull().GreaterThan(0);
            RuleFor(l => l.Temperatura).NotNull().GreaterThan(0);
            RuleFor(l => l.Luminosidad).NotNull().GreaterThan(0);
        }
    }
}
