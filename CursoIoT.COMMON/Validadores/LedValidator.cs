﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Validadores;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class LedValidator : ActuadorValidator<Led>
    {
        public LedValidator()
        {

            RuleFor(l => l.Encendido).NotNull();

        }
    }
}
