﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class PulsadorValidator:ActuadorValidator<Pulsador>
    {
        public PulsadorValidator()
        {
            RuleFor(p => p.Valor).NotNull();
        }
    }
}
