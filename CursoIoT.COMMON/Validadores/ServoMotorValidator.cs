﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class ServoMotorValidator:ActuadorValidator<Servomotor>
    {
        public ServoMotorValidator()
        {
            RuleFor(s => s.Angulo).NotNull().GreaterThan(0).LessThan(180);
        }
    }
}
