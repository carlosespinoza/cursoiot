﻿using CursoIoT.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.COMMON.Validadores
{
    public class UsuarioValidator:GenericValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(u => u.Apellidos).NotNull().NotEmpty();
            RuleFor(u => u.Email).EmailAddress().NotNull();
            RuleFor(u => u.Nombre).NotNull().NotEmpty();
            RuleFor(u => u.Password).NotEmpty().NotNull().MinimumLength(6);
            RuleFor(u => u.UserName).NotNull().NotEmpty();
        }
    }
}
