﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CursoIoT.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public GenericRepository(AbstractValidator<T> validator)
        {
            Validator = validator;
            client = new MongoClient(new MongoUrl(@"mongodb://cursoiot-user:iteshu2018@ds058048.mlab.com:58048/cursoiot"));
            db = client.GetDatabase("cursoiot");
            Resultado = false;
            Error = "";
        }
        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }
        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    Error = "";
                    Resultado = true;
                    return Collection().AsQueryable();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    Resultado = false;
                    return null;
                }

            }
        }

        public bool Resultado { get; set; }
        public string Error { get; set; }
        private AbstractValidator<T> Validator;
        private ValidationResult ResultadoDeValidacion;
        

        public T Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId();
            entidad.FechaHora = DateTime.Now;
            ResultadoDeValidacion = Validator.Validate(entidad);
            if (ResultadoDeValidacion.IsValid)
            {
                try
                {
                    Resultado = true;
                    Error = "";
                    Collection().InsertOne(entidad);
                    return entidad;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    Resultado = false;
                    return null;
                }
            }
            else
            {
                Resultado = false;
                Error = "Datos incompletos: ";
                foreach (var item in ResultadoDeValidacion.Errors)
                {
                    Error += item.ErrorMessage + "; ";
                }
                return null;
            }

        }

        public bool Delete(ObjectId id)
        {
            try
            {
                Error = "";
                Resultado = true;
                return Collection().DeleteOne(m => m.Id == id).DeletedCount == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                Resultado = false;
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            try
            {
                return Collection().Find(predicado).ToList().AsQueryable();
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return null;
            }
        }



        public T Update(T entidad)
        {
            try
            {
                entidad.FechaHora = DateTime.Now;
                return Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount == 1 ? entidad : null;
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return null;
            }
        }

        public T SearchById(ObjectId id)
        {
            try
            {
                return Collection().Find(p => p.Id == id).SingleOrDefault();
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return null;
            }
        }



        public bool DeleteAll(Expression<Func<T, bool>> predicado)
        {
            try
            {
                return Collection().DeleteMany(predicado).DeletedCount > 0;
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return false;
            }
        }

        public IEnumerable<T> CreateAll(IEnumerable<T> entidades)
        {
            try
            {
                Collection().InsertMany(entidades);
                Resultado = true;
                return entidades;
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return null;
            }
        }
    }
}
