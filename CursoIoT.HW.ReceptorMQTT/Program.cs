﻿using OpenNETCF.MQTT;
using System;
using System.Threading;

namespace CursoIoT.HW.ReceptorMQTT
{
    class Program
    {
        static void Main(string[] args)
        {
            int intentos = 0;
            MQTTClient client = new MQTTClient("52.162.236.17", 1883);
            Console.WriteLine("Intentando conectar ");
            client.MessageReceived += Client_MessageReceived;
            client.Connect("TestMQTT");
            while (!client.IsConnected)
            {
                Thread.Sleep(1000);
                Console.Write(".");
                intentos++;
                if (intentos == 120)
                {
                    Console.WriteLine("No se pudo conectar, revise la configuración o que el servidor este vivo...");
                    return;
                }
            }
            client.Subscriptions.Add(new Subscription("test"));
            Console.WriteLine("Conectado correctamente a MQTT, esperando mensajes; presione enter para terminar...");
            Console.ReadLine();
        }

        private static void Client_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            Console.WriteLine($"{topic}: {System.Text.Encoding.UTF8.GetString(payload)} ");
        }
    }
}
