﻿using OpenNETCF.MQTT;
using System;
using System.Threading;

namespace CursoIoT.HW.TestMQTT
{
    class Program
    {
        static void Main(string[] args)
        {
            int intentos=0;
            MQTTClient client = new MQTTClient("52.162.236.17", 1883);
            Console.WriteLine("Intentando conectar ");
            client.Connect("TestMQTT");
            while (!client.IsConnected)
            {
                Thread.Sleep(1000);
                Console.Write(".");
                intentos++;
                if (intentos == 120)
                {
                    Console.WriteLine("No se pudo conectar, revise la configuración o que el servidor este vivo...");
                    return;
                }
            }
            Console.WriteLine("Conectado correctamente a MQTT");
            string mensaje;
            do
            {
                Console.Write("Mensaje: ");
                mensaje = Console.ReadLine();
                if (mensaje != "")
                {
                    client.Publish("test", mensaje, QoS.FireAndForget, false);
                }
            } while (mensaje != "");
            Console.WriteLine("Aplicación terminada...");
        }

        
    }
}
