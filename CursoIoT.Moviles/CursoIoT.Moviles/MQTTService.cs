﻿using CursoIoT.BIZ;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursoIoT.Moviles
{
    public class MQTTService
    {
        private Mqtt mqtt;
        private string Id = "";
        public List<string> MensajesRecibidos;
        public event EventHandler<string> MensajeRecibido;
        public bool NoConectado { get; set; }

        public MQTTService(string id)
        {
            mqtt = new Mqtt("caegcursoiot.northcentralus.cloudapp.azure.com", 1883, "cursoiot", id,5);
            Id = id;
            mqtt.MensajeRecibido += Mqtt_MensajeRecibido;
            mqtt.Suscribirse("cursoiot/" + id);
            MensajesRecibidos = new List<string>();
            NoConectado = mqtt.ServidorMqttNoConectado;
        }

        private void Mqtt_MensajeRecibido(object sender, string e)
        {
            MensajesRecibidos.Add($"[{DateTime.Now.ToLongTimeString()}] {e}");
            MensajeRecibido(this, e);
        }

        public void CambiarStatusLed(bool status)
        {
            if (status)
            {
                mqtt.Publica("L1");
            }
            else
            {
                mqtt.Publica("L0");
            }
        }
        public void CambiarStatusBuzzer(bool status)
        {
            if (status)
            {
                mqtt.Publica("B1");
            }
            else
            {
                mqtt.Publica("B0");
            }
        }
        public void CambiarAngulo(float angulo)
        {
            mqtt.Publica($"M{angulo.ToString().PadLeft(3,'0')}");
        }

        internal void SolicitarActualizacion()
        {
            mqtt.Publica("AS");
        }
    }
}
