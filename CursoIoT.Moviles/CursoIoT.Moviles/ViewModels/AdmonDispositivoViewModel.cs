﻿using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursoIoT.Moviles.ViewModels
{
    public class AdmonDispositivoViewModel : ViewModelBase
    {
        private ILedManager ledManager;
        private ILecturaSensorManager lecturaSensorManager;
        private IBuzzerManager buzzerManager;
        private IPulsadorManager pulsadorManager;
        private IServoMotorManager servoMotorManager;
        private IDispositivoManager dispositivoManager;
        private MQTTService MQTTService;
        public bool ServidorMqttNoConectado { get; set; }

        public string URLMapa { get; set; }

        public bool LedStatus
        {
            get
            {
                if (Dispositivo != null && Dispositivo.EstadoLed != null)
                    return Dispositivo.EstadoLed.Encendido;
                else return false;
            }
            set { CambiaLed(value); }
        }

        public bool BuzzerStatus
        {
            get
            {
                if (Dispositivo != null && Dispositivo.EstadoBuzer != null)
                    return Dispositivo.EstadoBuzer.Encendido;
                else return false;
            }
            set { CambiaBuzzer(value); }
        }
        private float servoAngulo;
        public float ServoAngulo
        {
            get { return servoAngulo; }
            set { servoAngulo = value; }
        }

        public Dispositivo Dispositivo { get; set; }
        public ICommand VerLecturasSensoresCommand { get; set; }
        public ICommand VerLecturasPulsadorCommand { get; set; }
        public ICommand VerLecturasBuzzerCommand { get; set; }
        public ICommand VerLecturasLedCommand { get; set; }
        public ICommand VerLecturasServoCommand { get; set; }
        public ICommand MandarAnguloCommand { get; set; }
        public ICommand ActualizarSensoresCommand { get; set; }
        public ICommand EliminarDispositivoCommand { get; set; }

        public AdmonDispositivoViewModel()
        {

        }
        public AdmonDispositivoViewModel(Dispositivo dispositivo)
        {
            ledManager = FabricManager.LedManager();
            lecturaSensorManager = FabricManager.LecturaSensorManager();
            buzzerManager = FabricManager.BuzzerManager();
            pulsadorManager = FabricManager.PulsadorManager();
            servoMotorManager = FabricManager.ServoMotorManager();
            dispositivoManager = FabricManager.DispositivoManager();
            Dispositivo = dispositivo;
            VerLecturasBuzzerCommand = new Command(LecturasBuzzer);
            VerLecturasLedCommand = new Command(LecturasLed);
            VerLecturasPulsadorCommand = new Command(LecturasPulsador);
            VerLecturasSensoresCommand = new Command(LecturasSensores);
            VerLecturasServoCommand = new Command(LecturasServo);
            MandarAnguloCommand = new Command(CambiarAngulo);
            ActualizarSensoresCommand = new Command(ActualizarSensores);
            EliminarDispositivoCommand = new Command(EliminarDispositivo);
            MQTTService = new MQTTService(dispositivo.Id.ToString());
            MQTTService.MensajeRecibido += MQTTService_MensajeRecibido;
            ServidorMqttNoConectado = MQTTService.NoConectado;
            Notify("ServidorMqttNoConectado");
            URLMapa = $"https://www.google.com/maps/@{dispositivo.Latitud},{dispositivo.Longitud}";
            Notify("URLMapa");
        }

        private async void EliminarDispositivo()
        {
            var r = await messageService.MostrarAlertaConOpciones("¿Realmente desea eliminar este dispositivo y todas sus lecturas?, este proceso no se puede revertir", new List<string>()
            {
                "Si","No"
            });
            if (r == "Si")
            {
                lecturaSensorManager.EliminarTodos(l => l.IdDispositivo == Dispositivo.Id);
                ledManager.EliminarTodos(l => l.IdDispositivo == Dispositivo.Id);
                buzzerManager.EliminarTodos(l => l.IdDispositivo == Dispositivo.Id);
                pulsadorManager.EliminarTodos(l => l.IdDispositivo == Dispositivo.Id);
                servoMotorManager.EliminarTodos(l => l.IdDispositivo == Dispositivo.Id);
                dispositivoManager.Eliminar(Dispositivo.Id);
                await navigationService.NavigateToRegresar();
            }
        }

        private void ActualizarSensores(object obj)
        {
            MQTTService.SolicitarActualizacion();
        }

        private void CambiarAngulo()
        {
            MQTTService.CambiarAngulo(servoAngulo);
            Notify("ServoAngulo");

        }

        private void MQTTService_MensajeRecibido(object sender, string e)
        {
            if (e.Length > 20)
            {
                System.Threading.Tasks.Task.Delay(2000);
                Dispositivo = dispositivoManager.BuscarPorId(Dispositivo.Id);

                if (e.Contains("Sensores"))
                    Notify("Dispositivo");
                if (e.Contains("Servomotor"))
                    Notify("ServoAngulo");
                if (e.Contains("Pulso"))
                    Notify("Dispositivo");
                //if (e.Contains("Buzzer"))
                //    Notify("BuzzerStatus");
                //if (e.Contains("Led"))
                //    Notify("LedStatus");
            }
        }

        private void CambiaServo(int angulo)
        {
            MQTTService.CambiarAngulo(angulo);
        }

        private void CambiaBuzzer(bool status)
        {
            MQTTService.CambiarStatusBuzzer(status);
        }

        private void CambiaLed(bool status)
        {
            MQTTService.CambiarStatusLed(status);
        }

        private async void LecturasServo()
        {
            await navigationService.NavigateToMostrarEstadoDeServo(Dispositivo.Id);
        }

        private async void LecturasBuzzer()
        {
            await navigationService.NavigateToMostrarEstadoDeBuzzer(Dispositivo.Id);
        }

        private async void LecturasPulsador(object obj)
        {
            await navigationService.NavigateToMostrarValoresDePulsador(Dispositivo.Id);
        }

        private async void LecturasLed()
        {
            await navigationService.NavigateToMostrarEstadoDeLed(Dispositivo.Id);
        }

        private async void LecturasSensores()
        {
            await navigationService.NavigateToMostrarLecturasDeSensores(Dispositivo.Id);
        }
    }
}
