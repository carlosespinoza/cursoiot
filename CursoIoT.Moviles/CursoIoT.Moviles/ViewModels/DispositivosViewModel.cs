﻿using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursoIoT.Moviles.ViewModels
{
    public class DispositivosViewModel:ViewModelBase
    {
        private IDispositivoManager dispositivoManager;
        public Usuario Usuario { get; set; }
        private Dispositivo seleccionado;
        public Dispositivo DispositivoSeleccionado { get { return seleccionado; } set { seleccionado = value; Notify("DispositivoSeleccionado"); TabDispositivo(value); } }
        public List<Dispositivo> Dispositivos { get; set; }
        public ICommand TabDispositivoCommand { get; set; }
        public ICommand NuevoDispositivoCommand { get; set; }
        //public ICommand ActualizarDispositivos { get; set; }
        public DispositivosViewModel()
        {
                
        }
        public DispositivosViewModel(Usuario usuario)
        {
            dispositivoManager = FabricManager.DispositivoManager();
            NuevoDispositivoCommand = new Command(NuevoDispositivo);
            //ActualizarDispositivos = new Command(ActualizaDispositivos);
            Usuario = usuario;
            
        }

        public void ActualizaDispositivos()
        {
            Dispositivos = dispositivoManager.DispositivosDeUsuario(Usuario.Id).ToList();
            Notify("Dispositivos");
        }

        private async void NuevoDispositivo(object obj)
        {
            await navigationService.NavigateToNuevoDispositivo(Usuario);
        }

        private async void TabDispositivo(Dispositivo d)
        {
            if (d != null)
            {
                await messageService.MostrarAlerta("Conectando al servidor MQTT...");
                await navigationService.NavigateToAdmonDispositivo(d);
            }
        }
    }
}
