﻿using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using CursoIoT.Moviles.Views.Servicios;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursoIoT.Moviles.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private IUsuarioManager usuarioManager;
        private string nomUsuario;

        public string NomUsuario
        {
            get { return nomUsuario; }
            set { nomUsuario = value; this.Notify("NomUsuario"); }
        }
        private string pass;

        public string Pass
        {
            get { return pass; }
            set { pass = value; this.Notify("Pass"); }
        }
        public ICommand LoginCommand { get; set; }
        public ICommand RegistrarUsuarioCommand { get; set; }

        

        public LoginViewModel()
        {
            usuarioManager = FabricManager.UsuarioManager();
            LoginCommand = new Command(Login);
            RegistrarUsuarioCommand = new Command(NuevoUsuario);
            pass = "1234567";
            nomUsuario = "cespinoza";
           
        }

        public async void Login()
        {
            Usuario usuario = usuarioManager.Login(NomUsuario, pass);
            if (usuario != null)
            {
                await navigationService.NavigateToMostrarDispositivos(usuario);
            }
            else
            {
                await messageService.MostrarAlerta("Nombre de usuario y/o contraseña incorrecta...");
            }
        }
        public async void NuevoUsuario()
        {
            await navigationService.NavigateToNewUsuario();
        }


    }
}
