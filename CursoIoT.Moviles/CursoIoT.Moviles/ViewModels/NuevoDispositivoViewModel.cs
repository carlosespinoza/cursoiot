﻿using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursoIoT.Moviles.ViewModels
{
    public class NuevoDispositivoViewModel : ViewModelBase
    {
        private IDispositivoManager dispositivoManager;
        public Dispositivo Dispositivo { get; set; }
        public NuevoDispositivoViewModel()
        {

        }
        public NuevoDispositivoViewModel(Usuario usuario)
        {
            dispositivoManager = FabricManager.DispositivoManager();
            Dispositivo = new Dispositivo()
            {
                IdUsuario = usuario.Id
            };
            AgregarCommand = new Command(NuevoDispositivo);
        }

        private void NuevoDispositivo()
        {
            Dispositivo d = dispositivoManager.Insertar(Dispositivo);
            if (d != null)
            {
                messageService.MostrarAlerta($"Dispositivo creado correctamente con Id: {d.Id}");
                navigationService.NavigateToRegresar();
            }
            else
            {
                messageService.MostrarAlerta($"Error: {dispositivoManager.Error}");
            }
        }

        public ICommand AgregarCommand { get; set; }
    }
}
