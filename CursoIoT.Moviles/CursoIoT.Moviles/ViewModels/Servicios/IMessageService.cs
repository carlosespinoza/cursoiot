﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CursoIoT.Moviles.ViewModels.Servicios
{
    public interface IMessageService
    {
        Task MostrarAlerta(string mensaje);
        Task<string> MostrarAlertaConOpciones(string mensaje, List<string> opciones);
        Task<string> MostrarOpciones(string mensaje, List<string> opciones);
    }
}
