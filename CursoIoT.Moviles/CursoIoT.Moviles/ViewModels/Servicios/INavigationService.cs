﻿using CursoIoT.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CursoIoT.Moviles.ViewModels.Servicios
{
    public interface INavigationService
    {
        Task NavigateToMostrarDispositivos(Usuario usuario);
        Task NavigateToNewUsuario();
        Task NavigateToNuevoDispositivo(Usuario usuario);
        Task NavigateToMostrarLecturasDeSensores(ObjectId idDispositivo);
        Task NavigateToMostrarEstadoDeLed(ObjectId idDispositivo);
        Task NavigateToMostrarEstadoDeBuzzer(ObjectId idDispositivo);
        Task NavigateToMostrarEstadoDeServo(ObjectId idDispositivo);
        Task NavigateToMostrarValoresDePulsador(ObjectId idDispositivo);
        Task NavigateToAdmonDispositivo(Dispositivo dispositivoSeleccionado);
        Task NavigateToRegresar();
    }
}
