﻿using CursoIoT.Moviles.Views.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CursoIoT.Moviles.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected readonly Servicios.IMessageService messageService;
        protected readonly Servicios.INavigationService navigationService;
        public ViewModelBase()
        {
            messageService = new MessageService();
            navigationService = new NavigationService();
        }
        protected void Notify(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
