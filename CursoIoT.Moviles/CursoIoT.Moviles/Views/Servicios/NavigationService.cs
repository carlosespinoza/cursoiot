﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.Moviles.ViewModels.Servicios;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CursoIoT.Moviles.Views.Servicios
{
    public class NavigationService : INavigationService
    {
        public async Task NavigateToAdmonDispositivo(Dispositivo dispositivoSeleccionado)
        {
            await App.Current.MainPage.Navigation.PushAsync(new ViewAdmonDispositivo(dispositivoSeleccionado));
        }

        public async Task NavigateToMostrarDispositivos(Usuario usuario)
        {
            await App.Current.MainPage.Navigation.PushAsync(new ViewDispositivos(usuario));
        }

        public Task NavigateToMostrarEstadoDeBuzzer(ObjectId idDispositivo)
        {
            throw new NotImplementedException();
        }

        public Task NavigateToMostrarEstadoDeLed(ObjectId idDispositivo)
        {
            throw new NotImplementedException();
        }

        public Task NavigateToMostrarEstadoDeServo(ObjectId idDispositivo)
        {
            throw new NotImplementedException();
        }

        public Task NavigateToMostrarLecturasDeSensores(ObjectId idDispositivo)
        {
            throw new NotImplementedException();
        }

        public Task NavigateToMostrarValoresDePulsador(ObjectId idDispositivo)
        {
            throw new NotImplementedException();
        }

        public Task NavigateToNewUsuario()
        {
            throw new NotImplementedException();
        }

        public async Task NavigateToNuevoDispositivo(Usuario usuario)
        {
            await App.Current.MainPage.Navigation.PushAsync(new ViewNuevoDispositivo(usuario));
        }

        public async Task NavigateToRegresar()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
    }
}
