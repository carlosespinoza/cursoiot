﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CursoIoT.COMMON.Entidades;
using CursoIoT.Moviles.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursoIoT.Moviles.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewAdmonDispositivo : ContentPage
	{
		private Dispositivo dispositivo;

		public ViewAdmonDispositivo ()
		{
            InitializeComponent();
        }

		public ViewAdmonDispositivo(Dispositivo dispositivo)
		{
			InitializeComponent ();
			this.dispositivo = dispositivo;
            this.BindingContext = new AdmonDispositivoViewModel(dispositivo);
		}
	}
}