﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.Moviles.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursoIoT.Moviles.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewDispositivos : ContentPage
	{
        private Usuario usuario;
		public ViewDispositivos (Usuario usuario)
		{
			InitializeComponent ();
            this.usuario = usuario;
            this.BindingContext= new DispositivosViewModel(usuario);

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((DispositivosViewModel)BindingContext).ActualizaDispositivos();
        }
    }
}