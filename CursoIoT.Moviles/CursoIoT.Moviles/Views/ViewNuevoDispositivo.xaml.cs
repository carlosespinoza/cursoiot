﻿using CursoIoT.COMMON.Entidades;
using CursoIoT.Moviles.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursoIoT.Moviles.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewNuevoDispositivo : ContentPage
	{
		public ViewNuevoDispositivo (Usuario usuario)
		{
			InitializeComponent ();
			BindingContext = new NuevoDispositivoViewModel(usuario);
		}
	}
}