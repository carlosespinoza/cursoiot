﻿using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using MongoDB.Bson;
using System;

namespace CursoIoT.Receptor
{
    class Program
    {

        static void Main(string[] args)
        {
            Mqtt mqtt = new Mqtt("caegcursoiot.northcentralus.cloudapp.azure.com", 1883, "cursoiot", "receptor");
            mqtt.MensajeRecibido += Mqtt_MensajeRecibido;
            mqtt.Suscribirse("cursoiot/#");

            while (true)
            {

            }
        }

        private static void Mqtt_MensajeRecibido(object sender, string e)
        {
            try
            {
                if (e.Length > 24)
                {
                    IDispositivoManager dispositivoManager = FabricManager.DispositivoManager();
                    Console.Write($"{DateTime.Now.ToString()}: {e}");
                    string[] partes = e.Split(';');
                    Dispositivo dispositivo = dispositivoManager.BuscarPorId(ObjectId.Parse(partes[0]));
                    
                    switch (partes[1])
                    {
                        case "Sensores":
                            ILecturaSensorManager lecturaSensorManager = FabricManager.LecturaSensorManager();
                            LecturaSensores lectura = lecturaSensorManager.Insertar(new LecturaSensores()
                            {
                                Humedad = float.Parse(partes[3]),
                                Luminosidad = float.Parse(partes[4]),
                                Temperatura = float.Parse(partes[2]),
                                IdDispositivo = dispositivo.Id
                            });
                            if (lectura != null)
                            {
                                dispositivo.UltimaLectura = lectura;
                            }
                            break;
                        case "Pulso":
                            IPulsadorManager pulsadorManager = FabricManager.PulsadorManager();
                            Pulsador p = pulsadorManager.Insertar(new Pulsador()
                            {
                                IdDispositivo = dispositivo.Id,
                                Origen = "Hardware",
                                Valor = int.Parse(partes[2])
                            });
                            if (p != null)
                            {
                                dispositivo.ValorPulsador = p;
                            }
                            break;
                        case "Led":
                            ILedManager ledManager = FabricManager.LedManager();
                            Led l = ledManager.Insertar(new Led()
                            {
                                Encendido = partes[2] == "L1",
                                Origen = "Remoto",
                                IdDispositivo = dispositivo.Id

                            });
                            if (l != null)
                            {
                                dispositivo.EstadoLed = l;
                            }
                            break;
                        case "Buzzer":
                            IBuzzerManager buzzerManager = FabricManager.BuzzerManager();
                            Buzzer b = buzzerManager.Insertar(new Buzzer()
                            {
                                Encendido = partes[2] == "B1",
                                Origen = "Remoto",
                                IdDispositivo = dispositivo.Id
                            });
                            if (b != null)
                            {
                                dispositivo.EstadoBuzer = b;
                            }
                            break;
                        case "ServoMotor":
                            IServoMotorManager servoMotorManager = FabricManager.ServoMotorManager();
                            Servomotor s = servoMotorManager.Insertar(new Servomotor()
                            {
                                Angulo = float.Parse(partes[2]),
                                IdDispositivo = dispositivo.Id,
                                Origen = "Remoto"
                            });
                            if (s != null)
                            {
                                dispositivo.EstadoServomotor = s;
                            }
                            break;
                        default:
                            break;
                    }
                    ActualizarDispositivo(dispositivo, dispositivoManager);
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

        }

        private static void ActualizarDispositivo(Dispositivo dispositivo, IDispositivoManager dispositivoManager)
        {
            if (dispositivoManager.Actualizar(dispositivo) != null)
            {
                Console.WriteLine("Almacenada");
            }
            else
            {
                Console.WriteLine("Rechazada");
            }
        }
    }
}
