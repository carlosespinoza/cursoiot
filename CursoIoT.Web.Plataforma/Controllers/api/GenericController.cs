﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace CursoIoT.Web.Plataforma.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericController<T> : ControllerBase where T:BaseDTO
    {
        IGenericManager<T> manager;
        public GenericController(IGenericManager<T> manager)
        {
            this.manager = manager;
        }
        // GET: api/Generic
        [HttpGet]
        public IEnumerable<T> Get()
        {
            return manager.Listar;
        }

        // GET: api/Generic/5
        [HttpGet("{id}", Name = "Get")]
        public T Get(string id)
        {
            return manager.BuscarPorId(ObjectId.Parse(id));
        }

        // POST: api/Generic
        [HttpPost]
        public T Post([FromBody] T value)
        {
            return manager.Insertar(value);
        }

        // PUT: api/Generic/5
        [HttpPut("{id}")]
        public T Put(string id,[FromBody] T entidad)
        {
            if (id == entidad.Id.ToString())
            {
                return manager.Actualizar(entidad);
            }
            else
            {
                return null;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            return manager.Eliminar(ObjectId.Parse(id));
        }
    }
}
