﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//     //
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace ServiceReference1
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="UserDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    public partial class UserDTO : ServiceReference1.BaseDTO
    {
        
        private string EmailField;
        
        private bool IsActiveField;
        
        private bool IsAdminField;
        
        private string NameField;
        
        private string PasswordField;
        
        private string UserIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsActive
        {
            get
            {
                return this.IsActiveField;
            }
            set
            {
                this.IsActiveField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsAdmin
        {
            get
            {
                return this.IsAdminField;
            }
            set
            {
                this.IsAdminField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Password
        {
            get
            {
                return this.PasswordField;
            }
            set
            {
                this.PasswordField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserId
        {
            get
            {
                return this.UserIdField;
            }
            set
            {
                this.UserIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BaseDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceReference1.ProjectDTO))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceReference1.SensorDTO))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceReference1.ReadingDTO))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceReference1.AlertDTO))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceReference1.AlertSendDTO))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceReference1.UserDTO))]
    public partial class BaseDTO : object
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ProjectDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    public partial class ProjectDTO : ServiceReference1.BaseDTO
    {
        
        private string DescriptionField;
        
        private string ImageURLField;
        
        private string NameProjectField;
        
        private string ProjectIdField;
        
        private string UserIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ImageURL
        {
            get
            {
                return this.ImageURLField;
            }
            set
            {
                this.ImageURLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string NameProject
        {
            get
            {
                return this.NameProjectField;
            }
            set
            {
                this.NameProjectField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProjectId
        {
            get
            {
                return this.ProjectIdField;
            }
            set
            {
                this.ProjectIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserId
        {
            get
            {
                return this.UserIdField;
            }
            set
            {
                this.UserIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SensorDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    public partial class SensorDTO : ServiceReference1.BaseDTO
    {
        
        private string NotesField;
        
        private string ProjectIdField;
        
        private string SensorIdField;
        
        private string SensorNameField;
        
        private string UnitOfMeasureField;
        
        private string UserIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Notes
        {
            get
            {
                return this.NotesField;
            }
            set
            {
                this.NotesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProjectId
        {
            get
            {
                return this.ProjectIdField;
            }
            set
            {
                this.ProjectIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SensorId
        {
            get
            {
                return this.SensorIdField;
            }
            set
            {
                this.SensorIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SensorName
        {
            get
            {
                return this.SensorNameField;
            }
            set
            {
                this.SensorNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UnitOfMeasure
        {
            get
            {
                return this.UnitOfMeasureField;
            }
            set
            {
                this.UnitOfMeasureField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserId
        {
            get
            {
                return this.UserIdField;
            }
            set
            {
                this.UserIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ReadingDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    public partial class ReadingDTO : ServiceReference1.BaseDTO
    {
        
        private System.DateTime DateTimeField;
        
        private string ProjectIdField;
        
        private string ReadingIdField;
        
        private string SensorIdField;
        
        private double ValueField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateTime
        {
            get
            {
                return this.DateTimeField;
            }
            set
            {
                this.DateTimeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProjectId
        {
            get
            {
                return this.ProjectIdField;
            }
            set
            {
                this.ProjectIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ReadingId
        {
            get
            {
                return this.ReadingIdField;
            }
            set
            {
                this.ReadingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SensorId
        {
            get
            {
                return this.SensorIdField;
            }
            set
            {
                this.SensorIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AlertDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    public partial class AlertDTO : ServiceReference1.BaseDTO
    {
        
        private string AlertIDField;
        
        private string DescriptionField;
        
        private System.DateTime GrantDateField;
        
        private double LowerLimitField;
        
        private string MailToSendField;
        
        private string NombreField;
        
        private string ReadingIdField;
        
        private string SensorIdField;
        
        private string TextField;
        
        private double UpperLimitField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AlertID
        {
            get
            {
                return this.AlertIDField;
            }
            set
            {
                this.AlertIDField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime GrantDate
        {
            get
            {
                return this.GrantDateField;
            }
            set
            {
                this.GrantDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double LowerLimit
        {
            get
            {
                return this.LowerLimitField;
            }
            set
            {
                this.LowerLimitField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MailToSend
        {
            get
            {
                return this.MailToSendField;
            }
            set
            {
                this.MailToSendField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Nombre
        {
            get
            {
                return this.NombreField;
            }
            set
            {
                this.NombreField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ReadingId
        {
            get
            {
                return this.ReadingIdField;
            }
            set
            {
                this.ReadingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SensorId
        {
            get
            {
                return this.SensorIdField;
            }
            set
            {
                this.SensorIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Text
        {
            get
            {
                return this.TextField;
            }
            set
            {
                this.TextField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double UpperLimit
        {
            get
            {
                return this.UpperLimitField;
            }
            set
            {
                this.UpperLimitField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AlertSendDTO", Namespace="http://schemas.datacontract.org/2004/07/IoTCloud.Common.Entities")]
    public partial class AlertSendDTO : ServiceReference1.BaseDTO
    {
        
        private string AlertIdField;
        
        private string AlertSendIdField;
        
        private System.DateTime DateTimeField;
        
        private string NotesField;
        
        private string ReadingIdField;
        
        private string SensorIdField;
        
        private double ValueField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AlertId
        {
            get
            {
                return this.AlertIdField;
            }
            set
            {
                this.AlertIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AlertSendId
        {
            get
            {
                return this.AlertSendIdField;
            }
            set
            {
                this.AlertSendIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateTime
        {
            get
            {
                return this.DateTimeField;
            }
            set
            {
                this.DateTimeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Notes
        {
            get
            {
                return this.NotesField;
            }
            set
            {
                this.NotesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ReadingId
        {
            get
            {
                return this.ReadingIdField;
            }
            set
            {
                this.ReadingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SensorId
        {
            get
            {
                return this.SensorIdField;
            }
            set
            {
                this.SensorIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IoTCloudWCF")]
    public interface IoTCloudWCF
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/UsersByStatus", ReplyAction="http://tempuri.org/IoTCloudWCF/UsersByStatusResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.UserDTO>> UsersByStatusAsync(bool status);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddUser", ReplyAction="http://tempuri.org/IoTCloudWCF/AddUserResponse")]
        System.Threading.Tasks.Task<ServiceReference1.UserDTO> AddUserAsync(ServiceReference1.UserDTO user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/EditUser", ReplyAction="http://tempuri.org/IoTCloudWCF/EditUserResponse")]
        System.Threading.Tasks.Task<ServiceReference1.UserDTO> EditUserAsync(ServiceReference1.UserDTO user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteUser", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteUserResponse")]
        System.Threading.Tasks.Task<bool> DeleteUserAsync(ServiceReference1.UserDTO user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/ValidateUser", ReplyAction="http://tempuri.org/IoTCloudWCF/ValidateUserResponse")]
        System.Threading.Tasks.Task<bool> ValidateUserAsync(string email, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/FindUserById", ReplyAction="http://tempuri.org/IoTCloudWCF/FindUserByIdResponse")]
        System.Threading.Tasks.Task<ServiceReference1.UserDTO> FindUserByIdAsync(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/UserExist", ReplyAction="http://tempuri.org/IoTCloudWCF/UserExistResponse")]
        System.Threading.Tasks.Task<bool> UserExistAsync(string email);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/FindUserByEmail", ReplyAction="http://tempuri.org/IoTCloudWCF/FindUserByEmailResponse")]
        System.Threading.Tasks.Task<ServiceReference1.UserDTO> FindUserByEmailAsync(string email);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/ProjectsByUser", ReplyAction="http://tempuri.org/IoTCloudWCF/ProjectsByUserResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ProjectDTO>> ProjectsByUserAsync(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddProject", ReplyAction="http://tempuri.org/IoTCloudWCF/AddProjectResponse")]
        System.Threading.Tasks.Task<ServiceReference1.ProjectDTO> AddProjectAsync(ServiceReference1.ProjectDTO project);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/EditProject", ReplyAction="http://tempuri.org/IoTCloudWCF/EditProjectResponse")]
        System.Threading.Tasks.Task<ServiceReference1.ProjectDTO> EditProjectAsync(ServiceReference1.ProjectDTO project);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteProject", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteProjectResponse")]
        System.Threading.Tasks.Task<bool> DeleteProjectAsync(ServiceReference1.ProjectDTO project);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/FindProjectById", ReplyAction="http://tempuri.org/IoTCloudWCF/FindProjectByIdResponse")]
        System.Threading.Tasks.Task<ServiceReference1.ProjectDTO> FindProjectByIdAsync(string projectId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/SensorsByProject", ReplyAction="http://tempuri.org/IoTCloudWCF/SensorsByProjectResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.SensorDTO>> SensorsByProjectAsync(string projectId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddSensor", ReplyAction="http://tempuri.org/IoTCloudWCF/AddSensorResponse")]
        System.Threading.Tasks.Task<ServiceReference1.SensorDTO> AddSensorAsync(ServiceReference1.SensorDTO sensor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/EditSensor", ReplyAction="http://tempuri.org/IoTCloudWCF/EditSensorResponse")]
        System.Threading.Tasks.Task<ServiceReference1.SensorDTO> EditSensorAsync(ServiceReference1.SensorDTO sensor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteSensor", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteSensorResponse")]
        System.Threading.Tasks.Task<bool> DeleteSensorAsync(ServiceReference1.SensorDTO sensor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/SensorById", ReplyAction="http://tempuri.org/IoTCloudWCF/SensorByIdResponse")]
        System.Threading.Tasks.Task<ServiceReference1.SensorDTO> SensorByIdAsync(string sensorId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/ReadingsBySensor", ReplyAction="http://tempuri.org/IoTCloudWCF/ReadingsBySensorResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> ReadingsBySensorAsync(string sensorId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/ReadingsBySensorInTimeLapse", ReplyAction="http://tempuri.org/IoTCloudWCF/ReadingsBySensorInTimeLapseResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> ReadingsBySensorInTimeLapseAsync(string sensorId, System.DateTime startTime, System.DateTime endTime);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddReading", ReplyAction="http://tempuri.org/IoTCloudWCF/AddReadingResponse")]
        System.Threading.Tasks.Task<ServiceReference1.ReadingDTO> AddReadingAsync(ServiceReference1.ReadingDTO reading);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/EditReading", ReplyAction="http://tempuri.org/IoTCloudWCF/EditReadingResponse")]
        System.Threading.Tasks.Task<ServiceReference1.ReadingDTO> EditReadingAsync(ServiceReference1.ReadingDTO reading);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteReading", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteReadingResponse")]
        System.Threading.Tasks.Task<bool> DeleteReadingAsync(ServiceReference1.ReadingDTO reading);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/FindReadingById", ReplyAction="http://tempuri.org/IoTCloudWCF/FindReadingByIdResponse")]
        System.Threading.Tasks.Task<ServiceReference1.ReadingDTO> FindReadingByIdAsync(string readingId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddReadings", ReplyAction="http://tempuri.org/IoTCloudWCF/AddReadingsResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> AddReadingsAsync(System.Collections.Generic.List<ServiceReference1.ReadingDTO> readings);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteReadings", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteReadingsResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> DeleteReadingsAsync(System.Collections.Generic.List<ServiceReference1.ReadingDTO> readings);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AlertsBySensor", ReplyAction="http://tempuri.org/IoTCloudWCF/AlertsBySensorResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertDTO>> AlertsBySensorAsync(string sensorId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddAlert", ReplyAction="http://tempuri.org/IoTCloudWCF/AddAlertResponse")]
        System.Threading.Tasks.Task<ServiceReference1.AlertDTO> AddAlertAsync(ServiceReference1.AlertDTO alert);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/EditAlert", ReplyAction="http://tempuri.org/IoTCloudWCF/EditAlertResponse")]
        System.Threading.Tasks.Task<ServiceReference1.AlertDTO> EditAlertAsync(ServiceReference1.AlertDTO alert);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteAlert", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteAlertResponse")]
        System.Threading.Tasks.Task<bool> DeleteAlertAsync(ServiceReference1.AlertDTO alert);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/FindAlertById", ReplyAction="http://tempuri.org/IoTCloudWCF/FindAlertByIdResponse")]
        System.Threading.Tasks.Task<ServiceReference1.AlertDTO> FindAlertByIdAsync(string alertId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AlertsThatApplyToReading", ReplyAction="http://tempuri.org/IoTCloudWCF/AlertsThatApplyToReadingResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertDTO>> AlertsThatApplyToReadingAsync(string sensorId, string readingId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AlertsSendingsBySensor", ReplyAction="http://tempuri.org/IoTCloudWCF/AlertsSendingsBySensorResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertSendDTO>> AlertsSendingsBySensorAsync(string sensorId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AddAlertSend", ReplyAction="http://tempuri.org/IoTCloudWCF/AddAlertSendResponse")]
        System.Threading.Tasks.Task<ServiceReference1.AlertSendDTO> AddAlertSendAsync(ServiceReference1.AlertSendDTO alertSend);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/EditAlertSend", ReplyAction="http://tempuri.org/IoTCloudWCF/EditAlertSendResponse")]
        System.Threading.Tasks.Task<ServiceReference1.AlertSendDTO> EditAlertSendAsync(ServiceReference1.AlertSendDTO alertSend);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/DeleteAlertSend", ReplyAction="http://tempuri.org/IoTCloudWCF/DeleteAlertSendResponse")]
        System.Threading.Tasks.Task<bool> DeleteAlertSendAsync(ServiceReference1.AlertSendDTO alertSend);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/FindAlertSendById", ReplyAction="http://tempuri.org/IoTCloudWCF/FindAlertSendByIdResponse")]
        System.Threading.Tasks.Task<ServiceReference1.AlertSendDTO> FindAlertSendByIdAsync(string alertSendId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IoTCloudWCF/AlertSendByAlert", ReplyAction="http://tempuri.org/IoTCloudWCF/AlertSendByAlertResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertSendDTO>> AlertSendByAlertAsync(string alertId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface IoTCloudWCFChannel : ServiceReference1.IoTCloudWCF, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class IoTCloudWCFClient : System.ServiceModel.ClientBase<ServiceReference1.IoTCloudWCF>, ServiceReference1.IoTCloudWCF
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar el punto de conexión de servicio.
    /// </summary>
    /// <param name="serviceEndpoint">El punto de conexión para configurar</param>
    /// <param name="clientCredentials">Credenciales de cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public IoTCloudWCFClient() : 
                base(IoTCloudWCFClient.GetDefaultBinding(), IoTCloudWCFClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_IoTCloudWCF.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public IoTCloudWCFClient(EndpointConfiguration endpointConfiguration) : 
                base(IoTCloudWCFClient.GetBindingForEndpoint(endpointConfiguration), IoTCloudWCFClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public IoTCloudWCFClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(IoTCloudWCFClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public IoTCloudWCFClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(IoTCloudWCFClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public IoTCloudWCFClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.UserDTO>> UsersByStatusAsync(bool status)
        {
            return base.Channel.UsersByStatusAsync(status);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.UserDTO> AddUserAsync(ServiceReference1.UserDTO user)
        {
            return base.Channel.AddUserAsync(user);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.UserDTO> EditUserAsync(ServiceReference1.UserDTO user)
        {
            return base.Channel.EditUserAsync(user);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteUserAsync(ServiceReference1.UserDTO user)
        {
            return base.Channel.DeleteUserAsync(user);
        }
        
        public System.Threading.Tasks.Task<bool> ValidateUserAsync(string email, string password)
        {
            return base.Channel.ValidateUserAsync(email, password);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.UserDTO> FindUserByIdAsync(string userId)
        {
            return base.Channel.FindUserByIdAsync(userId);
        }
        
        public System.Threading.Tasks.Task<bool> UserExistAsync(string email)
        {
            return base.Channel.UserExistAsync(email);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.UserDTO> FindUserByEmailAsync(string email)
        {
            return base.Channel.FindUserByEmailAsync(email);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ProjectDTO>> ProjectsByUserAsync(string userId)
        {
            return base.Channel.ProjectsByUserAsync(userId);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ProjectDTO> AddProjectAsync(ServiceReference1.ProjectDTO project)
        {
            return base.Channel.AddProjectAsync(project);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ProjectDTO> EditProjectAsync(ServiceReference1.ProjectDTO project)
        {
            return base.Channel.EditProjectAsync(project);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteProjectAsync(ServiceReference1.ProjectDTO project)
        {
            return base.Channel.DeleteProjectAsync(project);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ProjectDTO> FindProjectByIdAsync(string projectId)
        {
            return base.Channel.FindProjectByIdAsync(projectId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.SensorDTO>> SensorsByProjectAsync(string projectId)
        {
            return base.Channel.SensorsByProjectAsync(projectId);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.SensorDTO> AddSensorAsync(ServiceReference1.SensorDTO sensor)
        {
            return base.Channel.AddSensorAsync(sensor);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.SensorDTO> EditSensorAsync(ServiceReference1.SensorDTO sensor)
        {
            return base.Channel.EditSensorAsync(sensor);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteSensorAsync(ServiceReference1.SensorDTO sensor)
        {
            return base.Channel.DeleteSensorAsync(sensor);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.SensorDTO> SensorByIdAsync(string sensorId)
        {
            return base.Channel.SensorByIdAsync(sensorId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> ReadingsBySensorAsync(string sensorId)
        {
            return base.Channel.ReadingsBySensorAsync(sensorId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> ReadingsBySensorInTimeLapseAsync(string sensorId, System.DateTime startTime, System.DateTime endTime)
        {
            return base.Channel.ReadingsBySensorInTimeLapseAsync(sensorId, startTime, endTime);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ReadingDTO> AddReadingAsync(ServiceReference1.ReadingDTO reading)
        {
            return base.Channel.AddReadingAsync(reading);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ReadingDTO> EditReadingAsync(ServiceReference1.ReadingDTO reading)
        {
            return base.Channel.EditReadingAsync(reading);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteReadingAsync(ServiceReference1.ReadingDTO reading)
        {
            return base.Channel.DeleteReadingAsync(reading);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ReadingDTO> FindReadingByIdAsync(string readingId)
        {
            return base.Channel.FindReadingByIdAsync(readingId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> AddReadingsAsync(System.Collections.Generic.List<ServiceReference1.ReadingDTO> readings)
        {
            return base.Channel.AddReadingsAsync(readings);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.ReadingDTO>> DeleteReadingsAsync(System.Collections.Generic.List<ServiceReference1.ReadingDTO> readings)
        {
            return base.Channel.DeleteReadingsAsync(readings);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertDTO>> AlertsBySensorAsync(string sensorId)
        {
            return base.Channel.AlertsBySensorAsync(sensorId);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.AlertDTO> AddAlertAsync(ServiceReference1.AlertDTO alert)
        {
            return base.Channel.AddAlertAsync(alert);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.AlertDTO> EditAlertAsync(ServiceReference1.AlertDTO alert)
        {
            return base.Channel.EditAlertAsync(alert);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteAlertAsync(ServiceReference1.AlertDTO alert)
        {
            return base.Channel.DeleteAlertAsync(alert);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.AlertDTO> FindAlertByIdAsync(string alertId)
        {
            return base.Channel.FindAlertByIdAsync(alertId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertDTO>> AlertsThatApplyToReadingAsync(string sensorId, string readingId)
        {
            return base.Channel.AlertsThatApplyToReadingAsync(sensorId, readingId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertSendDTO>> AlertsSendingsBySensorAsync(string sensorId)
        {
            return base.Channel.AlertsSendingsBySensorAsync(sensorId);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.AlertSendDTO> AddAlertSendAsync(ServiceReference1.AlertSendDTO alertSend)
        {
            return base.Channel.AddAlertSendAsync(alertSend);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.AlertSendDTO> EditAlertSendAsync(ServiceReference1.AlertSendDTO alertSend)
        {
            return base.Channel.EditAlertSendAsync(alertSend);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteAlertSendAsync(ServiceReference1.AlertSendDTO alertSend)
        {
            return base.Channel.DeleteAlertSendAsync(alertSend);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.AlertSendDTO> FindAlertSendByIdAsync(string alertSendId)
        {
            return base.Channel.FindAlertSendByIdAsync(alertSendId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ServiceReference1.AlertSendDTO>> AlertSendByAlertAsync(string alertId)
        {
            return base.Channel.AlertSendByAlertAsync(alertId);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IoTCloudWCF))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IoTCloudWCF))
            {
                return new System.ServiceModel.EndpointAddress("http://iotcloudv2.azurewebsites.net/IoTCloudWCF.svc");
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return IoTCloudWCFClient.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_IoTCloudWCF);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return IoTCloudWCFClient.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IoTCloudWCF);
        }
        
        public enum EndpointConfiguration
        {
            
            BasicHttpBinding_IoTCloudWCF,
        }
    }
}
