using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using CursoIoT.COMMON.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;

namespace CursoIoT_Test
{
    [TestClass]
    public class UnitTest1
    {
        IUsuarioManager usuarioManager = FabricManager.UsuarioManager();
        IDispositivoManager dispositivoManager = FabricManager.DispositivoManager();
        [TestMethod]
        public void CreaUsuario()
        {
            Usuario u = new Usuario()
            {
                Apellidos = "Espinoza",
                Email = "cespinoza@iteshu.edu.mx",
                Nombre = "Carlos",
                Password = "1234567",
                UserName = "cespinoza"
            };
            Usuario usuario = usuarioManager.Insertar(u);
            Assert.IsNotNull(u, usuarioManager.Error);
            //5c24f927f3e47665d829731a

        }
        [TestMethod]
        public void CreaDispositivo()
        {
            Dispositivo d = new Dispositivo()
            {
                Descripcion = "Dispositivo de prueba virtual",
                IdUsuario = ObjectId.Parse("5c24f927f3e47665d829731a"),
                Latitud = 20.3181681F,
                Longitud = -99.7075242F
            };
            Assert.IsNotNull(dispositivoManager.Insertar(d), dispositivoManager.Error);
            //5c254d3ced0dc527546ac25e fisico
            //5c14f2e78ab3bad1d42cf889 virtual
        }
    }
}
