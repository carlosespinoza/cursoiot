#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#include <Servo.h>
#define DHTTYPE DHT11
//Definición de pines
const int pinServo=D1;
const int pinPhotoResistencia=A0;
const int pinBuzzer=D2;
const int pinLed=D0;
const int pinPulsador=D7;
const int pinDHT=D4;
Servo servo;
DHT dht(pinDHT,DHTTYPE);




//Variables de apoyo
int buzzer=0;
void setup() {
  pinMode(pinServo,OUTPUT);
  pinMode(pinPhotoResistencia,INPUT);
  pinMode(pinBuzzer,OUTPUT);
  pinMode(pinLed,OUTPUT);
  pinMode(pinPulsador,INPUT);
  
  servo.attach(pinServo); //D1

  dht.begin();
  Serial.begin(9600);
  MueveServo(0);
  delay (2000);
}

void MueveServo(float angulo){
  servo.write(angulo);
  Serial.print("Servo a: ");
  Serial.println(angulo);
}

float LeePhotoResistencia(){
  float lectura=analogRead(pinPhotoResistencia);
  Serial.print("Luminosidad: ");
  Serial.println(lectura);
  return lectura;
}

void Sonar(bool sonar){
  if(sonar){
    digitalWrite(pinBuzzer,HIGH);
  }else{
    digitalWrite(pinBuzzer,LOW);
  }
}

void Led(bool encender){
  if(encender){
    digitalWrite(pinLed,HIGH);
  }else{
    digitalWrite(pinLed,LOW);
  }
}

bool DetectarPulso(){
  if(digitalRead(pinPulsador)==HIGH){
    Serial.println("Pulso");
    return true;
  }else{
    return false;
  }
}

float leerTemperatura(){
  float h=dht.readHumidity();
  Serial.print("Humedad: ");
  Serial.println(h);
  return h;
}

float leerHumedad(){
  float t=dht.readTemperature();
  Serial.print("Temperatura: ");
  Serial.println(t);
  return t;
}
void loop() {
  LeePhotoResistencia();
  leerTemperatura();
  leerHumedad();
  DetectarPulso();
  MueveServo(0);
  Sonar(true);
  Led(true);
  delay(1000);


  
  
  MueveServo(90);
  Sonar(false);
  Led(false);
  delay(1000);
  
}
