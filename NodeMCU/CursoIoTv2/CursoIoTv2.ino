#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Servo.h>
#define DHTTYPE DHT11
//Definición de pines
const int pinServo=D1;
const int pinPhotoResistencia=A0;
const int pinBuzzer=D2;
const int pinLed=D0;
const int pinPulsador=D7;
const int pinDHT=D4;
Servo servo;
DHT dht(pinDHT,DHTTYPE);

const char * idDispositivo = "5c254d3ced0dc527546ac25e";
// Casa
//const char *ssid = "INFINITUMB934D9";
//const char *password = "2C36E50AEF";
// Tlanalapa
const char *ssid = "HIDALGONORTE";
const char *password = "BOBBI2016";

const char* mqttServer = "caegcursoiot.northcentralus.cloudapp.azure.com";
const int mqttPort = 1883;
WiFiClient espClient;
PubSubClient client(espClient);
//Variables de apoyo
int buzzer=0;
String Data = "";
int segundos = 0;
int tiempo = 600 * 30;
int pulsos=0;
int eL=0;
int eB=0;
int anguloMotor=0;

void setup() {
  pinMode(pinServo,OUTPUT);
  pinMode(pinPhotoResistencia,INPUT);
  pinMode(pinBuzzer,OUTPUT);
  pinMode(pinLed,OUTPUT);
  pinMode(pinPulsador,INPUT);
  
  servo.attach(pinServo); //D1

  dht.begin();
  Serial.begin(9600);
  MueveServo(0);
  IniciarConexionWiFi();
  ConectarMQTT();
  delay (2000);
}

void ConectarMQTT() {
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);

  while (!client.connected()) {
    Serial.println("Conectando a MQTT...");

    if (client.connect("NodeMCU" )) {

      Serial.println("Conectado!!! :)");

    } else {

      Serial.print("Conexion fallada con estado: ");
      Serial.print(client.state());
      Serial.print(" esperando 2 segs.");
      delay(2000);
    }
    String trama=String("cursoiot/"+String(idDispositivo));
  int len = trama.length();
  char tema[len];
  trama.toCharArray(tema, len+1);
    client.subscribe(tema);
    PublicarMQTT("test","Dispositivo NodeMCU Carlos OnLine");
  }
} 
void MueveServo(int angulo){
  servo.write(angulo);
  Serial.print("Servo a: ");
  Serial.println(angulo);
  anguloMotor=angulo;
}
void callback(char* topic, byte * payload, unsigned int length) {

  Serial.print("Mensaje recibido en: ");
  Serial.println(topic);

  Serial.print("Mensaje:");
  String command = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    command = String(command + (char)payload[i]);
  }
  if(command=="B0"){
    Sonar(false);
  }
  if(command=="B1"){
    Sonar(true);
  }
  if(command=="L0"){
    Led(false);
  }
  if(command=="L1"){
    Led(true);
  }
  if(command=="AS"){
    MandaDatosSensores();
  }
  if(command=="S"){
    String data=String(String(idDispositivo)+";Estado;"+String(eL)+";"+String(eB)+";"+String(anguloMotor));
    PublicarMQTT("test",data);
  }
  if((char)payload[0]=='M'){
    char angulo[3];
    angulo[0]=(char)payload[1];
    angulo[1]=(char)payload[2];
    angulo[2]=(char)payload[3];
    int a=atoi(angulo);
    MueveServo(a);
    Data = String(String(idDispositivo) + ";Servomotor;" + String(a));
    PublicarMQTT("test", Data);
  }
}
void IniciarConexionWiFi() {

  WiFi.begin(ssid, password);
  Serial.print("Conectando SSID: ");
  Serial.println(ssid);
  while ( WiFi.status() != WL_CONNECTED)  //Intento de conexion a la red ssid
  {
    Serial.print(".");
    delay(500);
  }
  Serial.print("Conexion Establecida con ");
  Serial.println(ssid);
  Serial.print(" con la IP:");
  Serial.println(WiFi.localIP());
}



float LeePhotoResistencia(){
  float lectura=analogRead(pinPhotoResistencia);
  Serial.print("Luminosidad: ");
  Serial.println(lectura);
  return lectura;
}

void Sonar(bool sonar){
  Data = String(String(idDispositivo) + ";Buzzer;");
  if(sonar){
    digitalWrite(pinBuzzer,HIGH);
    eB=1;
    Data+=String("B1");
  }else{
    digitalWrite(pinBuzzer,LOW);
    eB=0;
    Data+=String("B0");
  }
  PublicarMQTT("test", Data);
}

void Led(bool encender){
  Data = String(String(idDispositivo) + ";Led;");
  if(encender){
    digitalWrite(pinLed,HIGH);
    eL=1;
    Data+=String("L1");
  }else{
    digitalWrite(pinLed,LOW);
    eL=0;
    Data+=String("L0");
  }
  PublicarMQTT("test", Data);
}

bool DetectarPulso(){
  if(digitalRead(pinPulsador)==HIGH){
    Serial.println("Pulso");
    return true;
  }else{
    return false;
  }
}

float LeerTemperatura(){
  float h=dht.readHumidity();
  Serial.print("Humedad: ");
  Serial.println(h);
  return h;
}

float LeerHumedad(){
  float t=dht.readTemperature();
  Serial.print("Temperatura: ");
  Serial.println(t);
  return t;
}


void PublicarMQTT(char * topic, String data){
  Serial.println(data);
  int len = data.length();
  char datos[len];
  data.toCharArray(datos, len+1);
   String trama=String("cursoiot/"+String(idDispositivo));
  len = trama.length();
  char tema[len];
  trama.toCharArray(tema, len+1);
  client.publish(tema, datos);
}
void MandaDatosSensores(){
  Data = String(String(idDispositivo) + ";Sensores;" + String(LeerTemperatura()) + ";" + String(LeerHumedad()) + ";" + String(LeePhotoResistencia()));
    PublicarMQTT("test", Data);
}

void loop() {
  client.loop();

  if (segundos == tiempo) {
	  segundos = 0;
	  MandaDatosSensores();
  }
  if (DetectarPulso()) {
	  pulsos++;
	  Data = String(String(idDispositivo) + ";Pulso;" + String(pulsos));
	  PublicarMQTT("test", Data);
   if(pulsos==32760){
    pulsos=0;
   }
  }

  segundos++;
  delay(100);
  
  
}
