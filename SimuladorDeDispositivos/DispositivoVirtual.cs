﻿using CursoIoT.BIZ;
using CursoIoT.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace SimuladorDeDispositivos
{
    public class DispositivoVirtual
    {
        Random r;
        public Mqtt Mqtt { get;private set; }
        public Dispositivo Dispositivo { get; private set; }
        public DispositivoVirtual(Dispositivo dispositivo,string server,int puerto, string topicRoot, int intervaloEnSegundos,int semilla)
        {
            Dispositivo = dispositivo;
            r = new Random(semilla);
            Mqtt = new Mqtt(server, puerto, topicRoot, dispositivo.Id.ToString());
            Mqtt.MensajeRecibido += Mqtt_MensajeRecibido;
            Mqtt.Suscribirse(topicRoot + "/" + dispositivo.Id.ToString() + "/#");
            while (true)
            {
                Thread.Sleep(intervaloEnSegundos * 1000);
                Dispositivo.UltimaLectura = new LecturaSensores()
                {
                    IdDispositivo = dispositivo.Id,
                    Humedad = r.Next(0, 100),
                    Luminosidad = r.Next(0, 255),
                    Temperatura = r.Next(0, 30)
                };
                Mqtt.Publica($"{Dispositivo.Id};Sensores;{dispositivo.UltimaLectura.Temperatura};{dispositivo.UltimaLectura.Humedad};{dispositivo.UltimaLectura.Luminosidad}");
            }
        }

        private void Mqtt_MensajeRecibido(object sender, string e)
        {
            Debug.WriteLine($"[{Dispositivo.Id.ToString()}]<={e}");
            switch (e)
            {
                case "B0":
                    Dispositivo.EstadoBuzer = new Buzzer()
                    {
                        Encendido = false,
                        Origen = "",
                        IdDispositivo = Dispositivo.Id
                    };
                break;
                case "B1":
                    Dispositivo.EstadoBuzer = new Buzzer()
                    {
                        Encendido = true,
                        Origen = "",
                        IdDispositivo = Dispositivo.Id
                    };
                    break;
                case "L0":
                    Dispositivo.EstadoLed = new Led()
                    {
                        Encendido = false,
                        Origen = "",
                        IdDispositivo = Dispositivo.Id
                    };
                    break;
                case "L1":
                    Dispositivo.EstadoLed = new Led()
                    {
                        Encendido = true,
                        Origen = "",
                        IdDispositivo = Dispositivo.Id
                    };
                    break;
                case "S":
                    Mqtt.Publica(Dispositivo.Id + ";Estado;" + BoolToInt(Dispositivo.EstadoLed.Encendido) + ";" + BoolToInt(Dispositivo.EstadoBuzer.Encendido) + ";" + Dispositivo.EstadoServomotor.Angulo.ToString());
                    break;
                default:
                    if (e[0] == 'M')
                    {
                        Dispositivo.EstadoServomotor = new Servomotor()
                        {
                            Angulo = int.Parse(e.Substring(1, 3)),
                            IdDispositivo = Dispositivo.Id,
                            Origen = "Remoto"
                        };
                    }
                    break;
            }
        }

        private string BoolToInt(bool encendido)
        {
            return encendido?"1":"0";
        }
    }
}
