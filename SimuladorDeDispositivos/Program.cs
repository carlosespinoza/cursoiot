﻿using CursoIoT.BIZ;
using MongoDB.Bson;
using System;
using System.Threading;

namespace SimuladorDeDispositivos
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Random r = new Random(DateTime.Now.Second);
            string id;
            int intervalo;
            Console.WriteLine("\t\tGenerador de dispositivos virtuales");
            Console.Write("Dirección del servidor MQTT: ");
            string server = Console.ReadLine();
            Console.Write("Puerto: ");
            int puerto = int.Parse(Console.ReadLine());
            Console.WriteLine("Proporcione los siguientes datos para simular un nuevo dispositivo, para terminar deje en blanco Id del dispositivo");
            do
            {
                Console.Write("Id del dispositivo a simular: ");
                id = Console.ReadLine();
                if (id != "")
                {
                    Console.Write("Intervalo de lecturas (seg): ");
                    intervalo = int.Parse(Console.ReadLine());
                    DispositivoVirtual d = new DispositivoVirtual(FabricManager.DispositivoManager().BuscarPorId(ObjectId.Parse(id)), server, puerto, "cursoiot", intervalo, r.Next());
                }
            } while (id!="");
        }

        private static void NuevoVirtual()
        {
            throw new NotImplementedException();
        }
        //static void Main(string[] args)
        //{
        //    Mqtt mqtt = new Mqtt("65.52.57.203", 1883, "cursoiot","prueba1");
        //    mqtt.MensajeRecibido += Mqtt_MensajeRecibido;
        //    mqtt.Suscribirse("cursoiot/#");
        //    string mensaje;
        //    do
        //    {
        //        Console.Write("Mensaje: ");
        //        mensaje = Console.ReadLine();
        //        mqtt.Publica(mensaje);
        //    } while (mensaje!="");
        //}

        //private static void Mqtt_MensajeRecibido(object sender, string e)
        //{
        //    Console.WriteLine("Recibido: " + e);
        //}
    }
}
